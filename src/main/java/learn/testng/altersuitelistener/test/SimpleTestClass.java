package learn.testng.altersuitelistener.test;

import org.testng.Assert;
import org.testng.annotations.Test;

public class SimpleTestClass {
	@Test(groups= {"all","FirstTest"})
	public void firstTest() {
		System.out.println("inside First test thread ID = "+Thread.currentThread().getId());
	}
	
	@Test(groups= {"all","secondtest"})
	public void secondTest(){
		System.out.println("inside Second test thread ID = "+Thread.currentThread().getId());
		Assert.assertTrue(false);
	}
	
	@Test(groups= {"all","thirdTest"})
	public void thirdTest(){
		System.out.println("inside third test thread ID = "+Thread.currentThread().getId());
	}
}
