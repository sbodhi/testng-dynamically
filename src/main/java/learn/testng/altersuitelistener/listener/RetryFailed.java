package learn.testng.altersuitelistener.listener;

import org.testng.IRetryAnalyzer;
import org.testng.ITestResult;

public class RetryFailed implements IRetryAnalyzer{
	int counter = 0;
	 int retryLimit = 3;
	public boolean retry(ITestResult result) {
		
		if(retryLimit>0) {
			--retryLimit;
			System.out.println();System.out.println();
			System.out.println("-------------------------------------------------");
			System.out.println("Retry attempt #"+ ++counter);
			System.out.println("-------------------------------------------------");
			System.out.println();System.out.println();
			return true;
		}else {
			return false;
		}				
	}

}
