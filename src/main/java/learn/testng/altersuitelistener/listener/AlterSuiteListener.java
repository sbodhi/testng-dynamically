package learn.testng.altersuitelistener.listener;

import java.util.ArrayList;
import java.util.List;

import org.testng.IAlterSuiteListener;
import org.testng.xml.XmlPackage;
import org.testng.xml.XmlSuite;
import org.testng.xml.XmlTest;
import org.testng.xml.XmlSuite.ParallelMode;

public class AlterSuiteListener implements IAlterSuiteListener{

	public void alter(List<XmlSuite> suites) {
		XmlSuite suite = suites.get(0);
		suite.setParallel(ParallelMode.METHODS);
		suite.setDataProviderThreadCount(5);
		suite.setThreadCount(5);
		suite.setVerbose(10);
		
		XmlTest xmlTest = new XmlTest(suite);
		xmlTest.setName("Testing");
		
		List <XmlPackage> packageNames = new ArrayList<XmlPackage>();
		packageNames.add(new XmlPackage("learn.testng.altersuitelistener.test.*"));
		xmlTest.setPackages(packageNames);
		
		List <String>includedGroups = new ArrayList<String>();
		includedGroups.add("all");		
		xmlTest.setIncludedGroups(includedGroups);		
	}

}
